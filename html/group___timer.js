var group___timer =
[
    [ "timer_config", "structtimer__config.html", [
      [ "period", "structtimer__config.html#a258408d6d5d13a24bfa5211d81ce1682", null ],
      [ "pFunc", "structtimer__config.html#a9cead290357aaae808d4db4ab87784df", null ],
      [ "timer", "structtimer__config.html#a0ff73b1428e9cd11a4f7a6cf46b7b550", null ]
    ] ],
    [ "TimerInit", "group___timer.html#ga148b01475111265d1798f5c204a93df0", null ],
    [ "TimerReset", "group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b", null ],
    [ "TimerStart", "group___timer.html#ga31487bffd934ce838a72f095f9231b24", null ],
    [ "TimerStop", "group___timer.html#gab652b899be3054eae4649a9063ec904b", null ]
];