var searchData=
[
  ['n_5394',['N',['../group___c_m_s_i_s__core___debug_functions.html#gabae0610bc2a97bbf7f689e953e0b451f',1,'APSR_Type::N()'],['../group___c_m_s_i_s__core___debug_functions.html#gabae0610bc2a97bbf7f689e953e0b451f',1,'APSR_Type::@0::N()'],['../group___c_m_s_i_s__core___debug_functions.html#gabae0610bc2a97bbf7f689e953e0b451f',1,'xPSR_Type::N()'],['../group___c_m_s_i_s__core___debug_functions.html#gabae0610bc2a97bbf7f689e953e0b451f',1,'xPSR_Type::@2::N()']]],
  ['nanoseconds_5395',['NANOSECONDS',['../struct_l_p_c___e_n_e_t___t.html#a509bec0d8aa03269d5e7f53eadad7fa0',1,'LPC_ENET_T']]],
  ['nanosecondsupdate_5396',['NANOSECONDSUPDATE',['../struct_l_p_c___e_n_e_t___t.html#ad72537d69dabab83f2b00dff9592498f',1,'LPC_ENET_T']]],
  ['nd1_5397',['ND1',['../struct_l_p_c___c_c_a_n___t.html#a65f32ab732a63e8469680b75ba8fd690',1,'LPC_CCAN_T']]],
  ['nd2_5398',['ND2',['../struct_l_p_c___c_c_a_n___t.html#a7cdfd206e6aee18d476f1494b788283e',1,'LPC_CCAN_T']]],
  ['not_5399',['NOT',['../struct_l_p_c___g_p_i_o___t.html#a9aa40dfddcb9ac35511ef18d693953af',1,'LPC_GPIO_T']]],
  ['npriv_5400',['nPRIV',['../group___c_m_s_i_s__core___debug_functions.html#ga2a6e513e8a6bf4e58db169e312172332',1,'CONTROL_Type::nPRIV()'],['../group___c_m_s_i_s__core___debug_functions.html#ga2a6e513e8a6bf4e58db169e312172332',1,'CONTROL_Type::@3::nPRIV()']]],
  ['nsel_5401',['nsel',['../struct_p_l_l___p_a_r_a_m___t.html#a4039a5f518d03f563944c3ca79285fca',1,'PLL_PARAM_T']]]
];
