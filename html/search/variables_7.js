var searchData=
[
  ['gdr_5191',['GDR',['../struct_l_p_c___a_d_c___t.html#a2272ff4c98c72be44cbb1f47f4bc3100',1,'LPC_ADC_T']]],
  ['ge_5192',['GE',['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'APSR_Type::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'APSR_Type::@0::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'xPSR_Type::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'xPSR_Type::@2::GE()']]],
  ['getcommfeature_5193',['GetCommFeature',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a49194845cd8fe069b674756634ddc8a3',1,'USBD_CDC_INIT_PARAM']]],
  ['getencpsresp_5194',['GetEncpsResp',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#aa59a8e34de6912875959b270de09160b',1,'USBD_CDC_INIT_PARAM']]],
  ['getmemsize_5195',['GetMemSize',['../struct_u_s_b_d___c_d_c___a_p_i.html#a66487e730c2a8648196f23f475067cdf',1,'USBD_CDC_API::GetMemSize()'],['../struct_u_s_b_d___d_f_u___a_p_i.html#a048df985da6f33c0a5c911b50c9f8e75',1,'USBD_DFU_API::GetMemSize()'],['../struct_u_s_b_d___h_i_d___a_p_i.html#a8f7a207918421d50c92bd23e37f494fe',1,'USBD_HID_API::GetMemSize()'],['../struct_u_s_b_d___h_w___a_p_i.html#a6b61fb5a82eab9e8f89aa906877f5dff',1,'USBD_HW_API::GetMemSize()'],['../struct_u_s_b_d___m_s_c___a_p_i.html#ab65dba1f2c48e61a83740793b2331ae3',1,'USBD_MSC_API::GetMemSize()']]],
  ['gpio_5196',['GPIO',['../struct_l_p_c___s_d_m_m_c___t.html#aa172d5c5fff46adafbd37892a1206432',1,'LPC_SDMMC_T']]],
  ['gpiopin_5197',['gpioPin',['../structdigital_i_o.html#aac0719041a981c2239101770e56567d6',1,'digitalIO']]],
  ['gpioport_5198',['gpioPort',['../structdigital_i_o.html#af3118cdc8c5891b2284ddf62a43b12c6',1,'digitalIO']]]
];
