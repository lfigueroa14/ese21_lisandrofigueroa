var searchData=
[
  ['gdr_1617',['GDR',['../struct_l_p_c___a_d_c___t.html#a2272ff4c98c72be44cbb1f47f4bc3100',1,'LPC_ADC_T']]],
  ['ge_1618',['GE',['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'APSR_Type::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'APSR_Type::@0::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'xPSR_Type::GE()'],['../group___c_m_s_i_s__core___debug_functions.html#gaa91800ec6e90e457c7a1acd1f2e17099',1,'xPSR_Type::@2::GE()']]],
  ['getcommfeature_1619',['GetCommFeature',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a49194845cd8fe069b674756634ddc8a3',1,'USBD_CDC_INIT_PARAM']]],
  ['getencpsresp_1620',['GetEncpsResp',['../struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#aa59a8e34de6912875959b270de09160b',1,'USBD_CDC_INIT_PARAM']]],
  ['getmemsize_1621',['GetMemSize',['../struct_u_s_b_d___m_s_c___a_p_i.html#ab65dba1f2c48e61a83740793b2331ae3',1,'USBD_MSC_API::GetMemSize()'],['../struct_u_s_b_d___h_w___a_p_i.html#a6b61fb5a82eab9e8f89aa906877f5dff',1,'USBD_HW_API::GetMemSize()'],['../struct_u_s_b_d___c_d_c___a_p_i.html#a66487e730c2a8648196f23f475067cdf',1,'USBD_CDC_API::GetMemSize()'],['../struct_u_s_b_d___d_f_u___a_p_i.html#a048df985da6f33c0a5c911b50c9f8e75',1,'USBD_DFU_API::GetMemSize()'],['../struct_u_s_b_d___h_i_d___a_p_i.html#a8f7a207918421d50c92bd23e37f494fe',1,'USBD_HID_API::GetMemSize()']]],
  ['gint0_5firqhandler_1622',['GINT0_IRQHandler',['../group___g_p_i_o.html#ga9030fad91603cf00d9b4d76a40e16434',1,'gpio.c']]],
  ['gint0_5firqn_1623',['GINT0_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a2b933af789374a01f2f5ad41e17d7eb5',1,'cmsis_43xx.h']]],
  ['gint1_5firqn_1624',['GINT1_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919ac19c1ef022589431663d3e53680410d6',1,'cmsis_43xx.h']]],
  ['gpdma_5fbsize_5f1_1625',['GPDMA_BSIZE_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gafd44c148b998d28bc156b947794ad011',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f128_1626',['GPDMA_BSIZE_128',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga808a32404cd29d656eb9ee31dacacdbb',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f16_1627',['GPDMA_BSIZE_16',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga6a56379136d5416a0799642fa2217fe2',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f256_1628',['GPDMA_BSIZE_256',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gad11cb83bebe2dc426f62cded244ea391',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f32_1629',['GPDMA_BSIZE_32',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2ebaf7a771f5bf603ecfed0503a66c5c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f4_1630',['GPDMA_BSIZE_4',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga768f1f0d1cf1a2611573362ed7b6a18d',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f64_1631',['GPDMA_BSIZE_64',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gababd7d98382dd69ae09a9a4b447a0977',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fbsize_5f8_1632',['GPDMA_BSIZE_8',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga21239562985215b67c024871f804f0bd',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fch_5fcfg_5ft_1633',['GPDMA_CH_CFG_T',['../struct_g_p_d_m_a___c_h___c_f_g___t.html',1,'']]],
  ['gpdma_5fch_5ft_1634',['GPDMA_CH_T',['../struct_g_p_d_m_a___c_h___t.html',1,'']]],
  ['gpdma_5fconn_5fadc_5f0_1635',['GPDMA_CONN_ADC_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaef5ce2dc7d43ce417fa2fb0540045eee',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fadc_5f1_1636',['GPDMA_CONN_ADC_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gacf1e0929b5098b0c53d0b41d95c2d7e8',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fdac_1637',['GPDMA_CONN_DAC',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga70d1a4fd7a27ad318e4f4d30a3e8e78d',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fi2s1_5frx_5fchannel_5f1_1638',['GPDMA_CONN_I2S1_Rx_Channel_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2c29873cef9813b13d69080c783ffc21',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fi2s1_5ftx_5fchannel_5f0_1639',['GPDMA_CONN_I2S1_Tx_Channel_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga8e1d14364364a15c61d47d644f1abed5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fi2s_5frx_5fchannel_5f1_1640',['GPDMA_CONN_I2S_Rx_Channel_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga0f3092d809bf78f82500d312ad0e349a',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fi2s_5ftx_5fchannel_5f0_1641',['GPDMA_CONN_I2S_Tx_Channel_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga1b6b56c58670486e2ffe6a0a70c5abce',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat0_5f0_1642',['GPDMA_CONN_MAT0_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga4be28419e516ded6affb01c411790f42',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat0_5f1_1643',['GPDMA_CONN_MAT0_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga87aa4ae7b9b68cc2c145d0ae6986edf2',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat1_5f0_1644',['GPDMA_CONN_MAT1_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gac6c7b9c39c1a03eb71f1f869b995cb1c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat1_5f1_1645',['GPDMA_CONN_MAT1_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaf0a360f1cd012b454bd28d89893e2121',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat2_5f0_1646',['GPDMA_CONN_MAT2_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga72ce3c7080bb473643f64f00ca6d0d37',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat2_5f1_1647',['GPDMA_CONN_MAT2_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga1fc900a69aeaf2ddadd72214650acc04',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat3_5f0_1648',['GPDMA_CONN_MAT3_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga15edb7c54b8ea2d2c8b0c2f762cc0ba6',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmat3_5f1_1649',['GPDMA_CONN_MAT3_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga222eda98e258b4b4f8ff61d5aa777276',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fmemory_1650',['GPDMA_CONN_MEMORY',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga976ea34bd6fc113b7dfcb2d1c6f5bffc',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fsct_5f0_1651',['GPDMA_CONN_SCT_0',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga12c736f56f0c09fdef50a6d177692158',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fsct_5f1_1652',['GPDMA_CONN_SCT_1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gac4e95b61a35b40087e9c705a69abc873',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fssp0_5frx_1653',['GPDMA_CONN_SSP0_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga35a189ac8738430fd2f0e83147483b39',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fssp0_5ftx_1654',['GPDMA_CONN_SSP0_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gab7a01050fe30674fa02bd3e9c1976819',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fssp1_5frx_1655',['GPDMA_CONN_SSP1_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga092169323edc65111c38376bded8ace4',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fssp1_5ftx_1656',['GPDMA_CONN_SSP1_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga05070551cd6af4e9a24775acfb5aacb5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart0_5frx_1657',['GPDMA_CONN_UART0_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2cb76be0ac39a2b019135810039376aa',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart0_5ftx_1658',['GPDMA_CONN_UART0_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gae2c7927807bbdd634bc1529dc9bce524',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart1_5frx_1659',['GPDMA_CONN_UART1_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaf0eea043e066244910dbe7608de732b5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart1_5ftx_1660',['GPDMA_CONN_UART1_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaea5294ba355bbe1efed07d65718c7e83',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart2_5frx_1661',['GPDMA_CONN_UART2_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gacdfdf222b40acd9c57260c7b493cc3b5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart2_5ftx_1662',['GPDMA_CONN_UART2_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga884e268ae363efcde9c3d5e791145cb1',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart3_5frx_1663',['GPDMA_CONN_UART3_Rx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga4c209fa5df563dc3b97333e34b24335d',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fconn_5fuart3_5ftx_1664',['GPDMA_CONN_UART3_Tx',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gab3d5bed081680418d665767b721bcc75',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmacconfig_5fe_1665',['GPDMA_DMACConfig_E',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga253822f2712564a42379a76d9447cde4',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmacconfig_5fm_1666',['GPDMA_DMACConfig_M',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga0f2a7e8c8704f5a897f911ac3a8617e3',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fa_1667',['GPDMA_DMACCxConfig_A',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gae28eb1e74ecafc5e6170e3f7a8688335',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fdestperipheral_1668',['GPDMA_DMACCxConfig_DestPeripheral',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga3b65fa1394d8c93789dd249c4e8a7568',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fe_1669',['GPDMA_DMACCxConfig_E',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga1c7608bb37d512277e42672ee4e785a5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fh_1670',['GPDMA_DMACCxConfig_H',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga987a4bb2d26cf2ef476483f347ad49ff',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fie_1671',['GPDMA_DMACCxConfig_IE',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga0fb4c3e9768c0a757b6ff25f77b75a26',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fitc_1672',['GPDMA_DMACCxConfig_ITC',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gac7c379cbf11a214f436620e4f7a7ee2a',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fl_1673',['GPDMA_DMACCxConfig_L',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga37a55c8ebde3d56defbf6281534237fe',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5fsrcperipheral_1674',['GPDMA_DMACCxConfig_SrcPeripheral',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaccb73c66c5349ec79a3f332d77554f0c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxconfig_5ftransfertype_1675',['GPDMA_DMACCxConfig_TransferType',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga63bace54233ed2446a1b442b46243833',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fdbsize_1676',['GPDMA_DMACCxControl_DBSize',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga18c40b7931f0b6cfe8b81f9a982c0641',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fdesttransuseahbmaster1_1677',['GPDMA_DMACCxControl_DestTransUseAHBMaster1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gacd71734a295849633110d6c64edda70c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fdi_1678',['GPDMA_DMACCxControl_DI',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaddcec41c911bbd0911391e6195c1c040',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fdwidth_1679',['GPDMA_DMACCxControl_DWidth',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga67bb6ed286afe6d4091c0fcd8799b451',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fi_1680',['GPDMA_DMACCxControl_I',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga6ef0b54b0190c139796679d6db1e6e19',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fprot1_1681',['GPDMA_DMACCxControl_Prot1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga883ee41e16f8df248437075cebab7993',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fprot2_1682',['GPDMA_DMACCxControl_Prot2',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gabe38faff26ee3951122c23ff1425d70a',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fprot3_1683',['GPDMA_DMACCxControl_Prot3',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaf6a439bbf5a4b082fa7f36effca23f15',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fsbsize_1684',['GPDMA_DMACCxControl_SBSize',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga1f5c9d534965c6a89ea22ca3fa48d859',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fsi_1685',['GPDMA_DMACCxControl_SI',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaa9b006e86536835dfe6f7034ee25d12a',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fsrctransuseahbmaster1_1686',['GPDMA_DMACCxControl_SrcTransUseAHBMaster1',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga90b29a1b2c82b32d6e586d74cdd727b7',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5fswidth_1687',['GPDMA_DMACCxControl_SWidth',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2ee63289c5e248a07ea901e233e1dd00',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fdmaccxcontrol_5ftransfersize_1688',['GPDMA_DMACCxControl_TransferSize',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga0e3ee35f724f4ef0cc8e91dfaec761e4',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fflow_5fcontrol_5ft_1689',['GPDMA_FLOW_CONTROL_T',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2cb59b641cd840f22780c44be1208133',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fnumber_5fchannels_1690',['GPDMA_NUMBER_CHANNELS',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gaf7c43b3d13c91c30ddf67e479966d5cd',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5fenabled_5fch_1691',['GPDMA_STAT_ENABLED_CH',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99cca711d88f645a203338f297a286b912767',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5fint_1692',['GPDMA_STAT_INT',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99ccaf89b610470a940617af6bef4f4451aa9',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5finterr_1693',['GPDMA_STAT_INTERR',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99ccaee7927c433e007f270c365bcca865706',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5finttc_1694',['GPDMA_STAT_INTTC',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99ccac093908914ed40148e81169fc15df2f7',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5frawinterr_1695',['GPDMA_STAT_RAWINTERR',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99ccad547775dc5510034932bf1597931e899',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstat_5frawinttc_1696',['GPDMA_STAT_RAWINTTC',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99cca4a74b1bc907fbbdbf9dfb05222f0b513',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstatclr_5finterr_1697',['GPDMA_STATCLR_INTERR',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ggabbb281ef4b818f2e60167cf766f94fdbaf6e975b9dfa7d659d18a377c7873b92a',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstatclr_5finttc_1698',['GPDMA_STATCLR_INTTC',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ggabbb281ef4b818f2e60167cf766f94fdba9a0c8256eb0f656d56f065914219c96c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstateclear_5ft_1699',['GPDMA_STATECLEAR_T',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gabbb281ef4b818f2e60167cf766f94fdb',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fstatus_5ft_1700',['GPDMA_STATUS_T',['../group___g_p_d_m_a__18_x_x__43_x_x.html#ga2f4aa97bd0ffa5046c8e2b17028d99cc',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fm2m_5fcontroller_5fdma_1701',['GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133ae5ff87adb4451f2a695c9d21c3c52c59',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fm2p_5fcontroller_5fdma_1702',['GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133a331474560497f41cfe921b0e55ce8722',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fm2p_5fcontroller_5fperipheral_1703',['GPDMA_TRANSFERTYPE_M2P_CONTROLLER_PERIPHERAL',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133a640177df7a3c696a9ccab9a09dcdbc0c',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fp2m_5fcontroller_5fdma_1704',['GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133aa2ae587fb924cb679f51250470927e34',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fp2m_5fcontroller_5fperipheral_1705',['GPDMA_TRANSFERTYPE_P2M_CONTROLLER_PERIPHERAL',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133aaeaf72b20cee326722ee7650405e2e43',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fp2p_5fcontroller_5fdestperipheral_1706',['GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DestPERIPHERAL',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133a176e307292918213de220bdae957ad6d',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fp2p_5fcontroller_5fdma_1707',['GPDMA_TRANSFERTYPE_P2P_CONTROLLER_DMA',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133a29ec59e967f3a1841002ef740552c1d5',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5ftransfertype_5fp2p_5fcontroller_5fsrcperipheral_1708',['GPDMA_TRANSFERTYPE_P2P_CONTROLLER_SrcPERIPHERAL',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gga2cb59b641cd840f22780c44be1208133a4615bdb6a415ddc02f8eab20a700a17d',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fwidth_5fbyte_1709',['GPDMA_WIDTH_BYTE',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gacc6deb5ab0e06eded3cdd151754db8f0',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fwidth_5fhalfword_1710',['GPDMA_WIDTH_HALFWORD',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gab28fc48561886a87a1c87eeb7078ef8b',1,'gpdma_18xx_43xx.h']]],
  ['gpdma_5fwidth_5fword_1711',['GPDMA_WIDTH_WORD',['../group___g_p_d_m_a__18_x_x__43_x_x.html#gad611897f330a6ec01b0699b244b132bb',1,'gpdma_18xx_43xx.h']]],
  ['gpio_1712',['GPIO',['../group___g_i_o_p.html',1,'(Global Namespace)'],['../struct_l_p_c___s_d_m_m_c___t.html#aa172d5c5fff46adafbd37892a1206432',1,'LPC_SDMMC_T::GPIO()'],['../group___g_p_i_o.html',1,'(Global Namespace)']]],
  ['gpio_2eh_1713',['gpio.h',['../gpio_8h.html',1,'']]],
  ['gpio0_5firqhandler_1714',['GPIO0_IRQHandler',['../group___g_p_i_o.html#gaa32175f2e875afc79e5e2df8fe065593',1,'gpio.c']]],
  ['gpio1_5firqhandler_1715',['GPIO1_IRQHandler',['../group___g_p_i_o.html#ga0c2ac16c0534feaab295ac82446374a6',1,'gpio.c']]],
  ['gpio2_5firqhandler_1716',['GPIO2_IRQHandler',['../group___g_p_i_o.html#ga249d7a560045daf5efd327072eec8669',1,'gpio.c']]],
  ['gpio3_5firqhandler_1717',['GPIO3_IRQHandler',['../group___g_p_i_o.html#ga0c399b311b42387909aa6db6f17ecc83',1,'gpio.c']]],
  ['gpio4_5firqhandler_1718',['GPIO4_IRQHandler',['../group___g_p_i_o.html#ga26bf79385d3aba4d2ac85072d11aa93c',1,'gpio.c']]],
  ['gpio5_5firqhandler_1719',['GPIO5_IRQHandler',['../group___g_p_i_o.html#gac51f861b652408b436462511631e28f8',1,'gpio.c']]],
  ['gpio6_5firqhandler_1720',['GPIO6_IRQHandler',['../group___g_p_i_o.html#ga6879239e64ad915b5733430d98fc81c0',1,'gpio.c']]],
  ['gpio7_5firqhandler_1721',['GPIO7_IRQHandler',['../group___g_p_i_o.html#ga60af99d217ffff705501227185a7389b',1,'gpio.c']]],
  ['gpio_5f1_1722',['GPIO_1',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a8595f8d7a0ae611fb06e4b9c690295f3',1,'gpio.h']]],
  ['gpio_5f3_1723',['GPIO_3',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a426d08ab85ab3aebbb53eed1a32dfb71',1,'gpio.h']]],
  ['gpio_5f5_1724',['GPIO_5',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a6213ab680cde9fe00f518cfb26a30a6b',1,'gpio.h']]],
  ['gpio_5flcd_5f1_1725',['GPIO_LCD_1',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a5c5082a554252716e298a652ad87439d',1,'gpio.h']]],
  ['gpio_5flcd_5f2_1726',['GPIO_LCD_2',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a5810f50ee9e0ac4e2747e39c15c9f292',1,'gpio.h']]],
  ['gpio_5flcd_5f3_1727',['GPIO_LCD_3',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a759f3c48e0be876fb1c41dc5486a032b',1,'gpio.h']]],
  ['gpio_5flcd_5f4_1728',['GPIO_LCD_4',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a456c4185a2d478067d05356f722c64fa',1,'gpio.h']]],
  ['gpio_5flcd_5fen_1729',['GPIO_LCD_EN',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a79d442dd993c5dc665b171b9687e75a1',1,'gpio.h']]],
  ['gpio_5flcd_5frs_1730',['GPIO_LCD_RS',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1ac4a765446c9688e70157619e9270d1dc',1,'gpio.h']]],
  ['gpio_5fled_5f1_1731',['GPIO_LED_1',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1af9d189ee1162202f52d6f144d6dbcd70',1,'gpio.h']]],
  ['gpio_5fled_5f2_1732',['GPIO_LED_2',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a25c91d38cf3578fa1478645dd3ff1e5c',1,'gpio.h']]],
  ['gpio_5fled_5f3_1733',['GPIO_LED_3',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1ae668df8313c1cdc2b859163b9a1b9683',1,'gpio.h']]],
  ['gpio_5fled_5frgb_5fb_1734',['GPIO_LED_RGB_B',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1ae3f8395a1315494b633272a7692e4cb6',1,'gpio.h']]],
  ['gpio_5fled_5frgb_5fg_1735',['GPIO_LED_RGB_G',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a5a98b5928a8e541b369e2c178e204a46',1,'gpio.h']]],
  ['gpio_5fled_5frgb_5fr_1736',['GPIO_LED_RGB_R',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a462756dbe3f1190acc532bd237cbbf45',1,'gpio.h']]],
  ['gpio_5ft_1737',['gpio_t',['../group___g_i_o_p.html#ga6baa390d39e68032f8ebed93a7039be1',1,'gpio.h']]],
  ['gpio_5ft_5fcol0_1738',['GPIO_T_COL0',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a74bdd8649ed058b6d8f84a57ee12c4fe',1,'gpio.h']]],
  ['gpio_5ft_5fcol1_1739',['GPIO_T_COL1',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1aeb08f8adb2ce6cd211226be46cb105ca',1,'gpio.h']]],
  ['gpio_5ft_5ffil0_1740',['GPIO_T_FIL0',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a802cc80984a7fc54974680a3c97cd54b',1,'gpio.h']]],
  ['gpio_5ft_5ffil2_1741',['GPIO_T_FIL2',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a2f561564cf4db4ece05338a9b370cc45',1,'gpio.h']]],
  ['gpio_5ft_5ffil3_1742',['GPIO_T_FIL3',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a32c94a5e0853d834f10449600e6764b4',1,'gpio.h']]],
  ['gpio_5ftec_5f1_1743',['GPIO_TEC_1',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a7c6aae0c032959af3ad89c1c276f37b0',1,'gpio.h']]],
  ['gpio_5ftec_5f2_1744',['GPIO_TEC_2',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a792d19419bb5688050556ed9a76b6697',1,'gpio.h']]],
  ['gpio_5ftec_5f3_1745',['GPIO_TEC_3',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1ac14272ffdab3e7d4ffabaa3ac9e364f5',1,'gpio.h']]],
  ['gpio_5ftec_5f4_1746',['GPIO_TEC_4',['../group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a74d98f13289cf181aeb1747a557dd20b',1,'gpio.h']]],
  ['gpioactivgroupint_1747',['GPIOActivGroupInt',['../group___g_i_o_p.html#ga7cd6dcd214c5c1e71daf24c9d93a237b',1,'GPIOActivGroupInt(gpiogroupgp_t group_gp, gpio_t *pins, uint8_t n_pins, void *ptr_int_func, bool edge):&#160;gpio.c'],['../group___g_p_i_o.html#ga7cd6dcd214c5c1e71daf24c9d93a237b',1,'GPIOActivGroupInt(gpiogroupgp_t group_gp, gpio_t *pins, uint8_t n_pins, void *ptr_int_func, bool edge):&#160;gpio.c']]],
  ['gpioactivint_1748',['GPIOActivInt',['../group___g_i_o_p.html#ga57e4924b8d8682ab6b41fd71fdd36d63',1,'GPIOActivInt(gpiogp_t gp, gpio_t pin, void *ptr_int_func, bool edge):&#160;gpio.c'],['../group___g_p_i_o.html#ga57e4924b8d8682ab6b41fd71fdd36d63',1,'GPIOActivInt(gpiogp_t gp, gpio_t pin, void *ptr_int_func, bool edge):&#160;gpio.c']]],
  ['gpiocleanint_1749',['GPIOCleanInt',['../group___g_i_o_p.html#ga64300177067f5d3e97aa7355d137fead',1,'gpio.h']]],
  ['gpiodeinit_1750',['GPIODeinit',['../group___g_i_o_p.html#gaa3ab377728bae4c84463cb75c266b60b',1,'GPIODeinit(void):&#160;gpio.c'],['../group___g_p_i_o.html#gaa3ab377728bae4c84463cb75c266b60b',1,'GPIODeinit(void):&#160;gpio.c']]],
  ['gpiogp0_1751',['GPIOGP0',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a337eb35784454571aa5205d3c9a5ef54',1,'gpio.h']]],
  ['gpiogp1_1752',['GPIOGP1',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a082ed53eb76f9f451303066587030dab',1,'gpio.h']]],
  ['gpiogp2_1753',['GPIOGP2',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351afdd81f1a361d56ca4130aec365b8dd56',1,'gpio.h']]],
  ['gpiogp3_1754',['GPIOGP3',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a627f2290a819d773821a7a92684435c8',1,'gpio.h']]],
  ['gpiogp4_1755',['GPIOGP4',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a3aabc56dfb937c043170e354c5f76b58',1,'gpio.h']]],
  ['gpiogp5_1756',['GPIOGP5',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a79c06078e97f9cb8511ab39716fca7f2',1,'gpio.h']]],
  ['gpiogp6_1757',['GPIOGP6',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351ad4fb6427bf2bc5bfe83927ef7cefef8c',1,'gpio.h']]],
  ['gpiogp7_1758',['GPIOGP7',['../group___g_i_o_p.html#gga3b29464441a317a4573a0910b6e59351a42e372506f8f918e59e9f198bb790d8a',1,'gpio.h']]],
  ['gpiogp_5ft_1759',['gpiogp_t',['../group___g_i_o_p.html#ga3b29464441a317a4573a0910b6e59351',1,'gpio.h']]],
  ['gpiogr_5fcomb_1760',['GPIOGR_COMB',['../group___g_p_i_o_g_p__18_x_x__43_x_x.html#ga99839b878e80485d41a8ed02c16a9aaf',1,'gpiogroup_18xx_43xx.h']]],
  ['gpiogr_5fint_1761',['GPIOGR_INT',['../group___g_p_i_o_g_p__18_x_x__43_x_x.html#gaf15160a16e7ec36a9046ad41d9b5e85b',1,'gpiogroup_18xx_43xx.h']]],
  ['gpiogr_5ftrig_1762',['GPIOGR_TRIG',['../group___g_p_i_o_g_p__18_x_x__43_x_x.html#gad93f80c7a388c505b90dd0a8daa2f6b4',1,'gpiogroup_18xx_43xx.h']]],
  ['gpiogroupgp0_1763',['GPIOGROUPGP0',['../group___g_i_o_p.html#ggaf91730be0b90f4abf2e8bbc227b496a0a5462b6dde98f1150bf2595b89359623e',1,'gpio.h']]],
  ['gpiogroupgp1_1764',['GPIOGROUPGP1',['../group___g_i_o_p.html#ggaf91730be0b90f4abf2e8bbc227b496a0a5f7595b7e15733e15c96faeab7b77708',1,'gpio.h']]],
  ['gpiogroupgp_5ft_1765',['gpiogroupgp_t',['../group___g_i_o_p.html#gaf91730be0b90f4abf2e8bbc227b496a0',1,'gpio.h']]],
  ['gpiooff_1766',['GPIOOff',['../group___g_i_o_p.html#ga30ed9d97406129b9b6390154a7479d70',1,'GPIOOff(gpio_t pin):&#160;gpio.c'],['../group___g_p_i_o.html#ga30ed9d97406129b9b6390154a7479d70',1,'GPIOOff(gpio_t pin):&#160;gpio.c']]],
  ['gpioon_1767',['GPIOOn',['../group___g_i_o_p.html#gaba62250f2b0e73a98b6f35fb6078fdb1',1,'GPIOOn(gpio_t pin):&#160;gpio.c'],['../group___g_p_i_o.html#gaba62250f2b0e73a98b6f35fb6078fdb1',1,'GPIOOn(gpio_t pin):&#160;gpio.c']]],
  ['gpiopin_1768',['gpioPin',['../structdigital_i_o.html#aac0719041a981c2239101770e56567d6',1,'digitalIO']]],
  ['gpioport_1769',['gpioPort',['../structdigital_i_o.html#af3118cdc8c5891b2284ddf62a43b12c6',1,'digitalIO']]],
  ['gpioread_1770',['GPIORead',['../group___g_i_o_p.html#gab42a477acc6064150000fb4a1bd4305c',1,'GPIORead(gpio_t pin):&#160;gpio.c'],['../group___g_p_i_o.html#gab42a477acc6064150000fb4a1bd4305c',1,'GPIORead(gpio_t pin):&#160;gpio.c']]],
  ['gpiotoggle_1771',['GPIOToggle',['../group___g_i_o_p.html#gacf7eae149edbe60c38faeb4b634b75b6',1,'GPIOToggle(gpio_t pin):&#160;gpio.c'],['../group___g_p_i_o.html#gacf7eae149edbe60c38faeb4b634b75b6',1,'GPIOToggle(gpio_t pin):&#160;gpio.c']]]
];
