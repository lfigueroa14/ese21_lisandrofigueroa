var searchData=
[
  ['pal_2608',['PAL',['../struct_l_p_c___l_c_d___t.html#ae68b7ee8bf7bbe3927b79cbf7849b4c3',1,'LPC_LCD_T']]],
  ['panaloginput_2609',['pAnalogInput',['../structanalog__input__config.html#a2298b64ab1835d56f927cb94137f36ad',1,'analog_input_config']]],
  ['param_5fevrt_5fsource_2610',['PARAM_EVRT_SOURCE',['../group___e_v_r_t__18_x_x__43_x_x.html#ga2475491cb73d4089705caa28008c8634',1,'evrt_18xx_43xx.h']]],
  ['param_5fevrt_5fsource_5factive_5ftype_2611',['PARAM_EVRT_SOURCE_ACTIVE_TYPE',['../group___e_v_r_t__18_x_x__43_x_x.html#ga258c7be7764bf695a3e958d2caee184a',1,'evrt_18xx_43xx.h']]],
  ['pc_2612',['PC',['../struct_l_p_c___t_i_m_e_r___t.html#a84724e7860a32fe120a1627483dfa5c5',1,'LPC_TIMER_T']]],
  ['pcsr_2613',['PCSR',['../group___c_m_s_i_s__core___debug_functions.html#gaccef6b622c8a41342ed32345b0922bea',1,'DWT_Type']]],
  ['pd0_5fsleep0_5fhw_5fena_2614',['PD0_SLEEP0_HW_ENA',['../struct_l_p_c___p_m_c___t.html#a179694e15b55b5bc4d82b43a61f6def6',1,'LPC_PMC_T']]],
  ['pd0_5fsleep0_5fmode_2615',['PD0_SLEEP0_MODE',['../struct_l_p_c___p_m_c___t.html#a316f4f2e318a0caf15cb8e2f9fa51ab4',1,'LPC_PMC_T']]],
  ['pendsv_5firqn_2616',['PendSV_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a03c3cc89984928816d81793fc7bce4a2',1,'cmsis_43xx.h']]],
  ['period_2617',['period',['../structtimer__config.html#a258408d6d5d13a24bfa5211d81ce1682',1,'timer_config']]],
  ['periodiclistbase_2618',['PERIODICLISTBASE',['../struct_l_p_c___u_s_b_h_s___t.html#a220024dbd2ce8566ae29c4fd779bdfea',1,'LPC_USBHS_T']]],
  ['pfi_2619',['PFI',['../group___l_p_c___types___public___types.html#ga5cad251913e41ad7a8c765945356ec47',1,'lpc_types.h']]],
  ['pfr_2620',['PFR',['../group___c_m_s_i_s__core___debug_functions.html#ga00a6649cfac6bbadee51d6ba4c73001d',1,'SCB_Type']]],
  ['pfunc_2621',['pFunc',['../structtimer__config.html#a9cead290357aaae808d4db4ab87784df',1,'timer_config']]],
  ['pfv_2622',['PFV',['../group___l_p_c___types___public___types.html#ga5f7b826e88ffb1fae2260abb3a75b19a',1,'lpc_types.h']]],
  ['pid0_2623',['PID0',['../group___c_m_s_i_s__core___debug_functions.html#gad6c87ae4ca1aa56b4369a97fca639926',1,'ITM_Type']]],
  ['pid1_2624',['PID1',['../group___c_m_s_i_s__core___debug_functions.html#gae554433b6f6c4733d222bcb2c75ccb39',1,'ITM_Type']]],
  ['pid2_2625',['PID2',['../group___c_m_s_i_s__core___debug_functions.html#gaf07d9a44e0188d55742f5d6a8752cd2c',1,'ITM_Type']]],
  ['pid3_2626',['PID3',['../group___c_m_s_i_s__core___debug_functions.html#ga510fcf8ad6966fdfb0767e624b74c64f',1,'ITM_Type']]],
  ['pid4_2627',['PID4',['../group___c_m_s_i_s__core___debug_functions.html#gad75960b83ea47a469e6a1406dd9eefa6',1,'ITM_Type']]],
  ['pid5_2628',['PID5',['../group___c_m_s_i_s__core___debug_functions.html#ga7276a30c464f0b34944b6eb16d3df077',1,'ITM_Type']]],
  ['pid6_2629',['PID6',['../group___c_m_s_i_s__core___debug_functions.html#gae5d83564471b76d88088a949ca67ac9b',1,'ITM_Type']]],
  ['pid7_2630',['PID7',['../group___c_m_s_i_s__core___debug_functions.html#ga247fae2f4a140d4da5e8a044370dedec',1,'ITM_Type']]],
  ['pin_2631',['PIN',['../struct_l_p_c___g_p_i_o___t.html#a2a7d9cae51250ad1c8b30cdb35f649ea',1,'LPC_GPIO_T']]],
  ['pin_2632',['pin',['../struct_s_p_i___address__t.html#ab40a673fb19c1e650e1f79de91788aa5',1,'SPI_Address_t']]],
  ['pin_5fint0_5firqn_2633',['PIN_INT0_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919accb1d16eecce69aeafc540ec4524a243',1,'cmsis_43xx.h']]],
  ['pin_5fint1_5firqn_2634',['PIN_INT1_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919ad40e7c35a92f95e5b806c2e29300a4ea',1,'cmsis_43xx.h']]],
  ['pin_5fint2_5firqn_2635',['PIN_INT2_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919adba51725fa9a83db8d7b79e660a73de7',1,'cmsis_43xx.h']]],
  ['pin_5fint3_5firqn_2636',['PIN_INT3_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919adda195917b256f4fe190edd29f4d0967',1,'cmsis_43xx.h']]],
  ['pin_5fint4_5firqn_2637',['PIN_INT4_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a5c766577f50ed37f5833a60ca2ee6685',1,'cmsis_43xx.h']]],
  ['pin_5fint5_5firqn_2638',['PIN_INT5_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a6a46b645dc4519ea6b2944cb05edd795',1,'cmsis_43xx.h']]],
  ['pin_5fint6_5firqn_2639',['PIN_INT6_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919ae7dc20efbb8225c27c1c89f2fe684b1c',1,'cmsis_43xx.h']]],
  ['pin_5fint7_5firqn_2640',['PIN_INT7_IRQn',['../group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919af90578359dcf92275d7466386edebbc9',1,'cmsis_43xx.h']]],
  ['pinintch0_2641',['PININTCH0',['../group___p_i_n_i_n_t__18_x_x__43_x_x.html#gaba419aacec8ac614ec6d121e05bf79cd',1,'pinint_18xx_43xx.h']]],
  ['pinmux_5fgrp_5ft_2642',['PINMUX_GRP_T',['../struct_p_i_n_m_u_x___g_r_p___t.html',1,'']]],
  ['pintsel_2643',['PINTSEL',['../struct_l_p_c___s_c_u___t.html#a8ef8b92ccfb8598c38506400a44c1d4e',1,'LPC_SCU_T']]],
  ['pldmnd_2644',['PLDMND',['../struct_l_p_c___s_d_m_m_c___t.html#af136fdd14b2c1bc6535894b4a4056312',1,'LPC_SDMMC_T']]],
  ['pll_2645',['PLL',['../struct_l_p_c___c_g_u___t.html#aad5b65eccb2424bc90575cafe7d38d29',1,'LPC_CGU_T']]],
  ['pll0audio_5ffrac_2646',['PLL0AUDIO_FRAC',['../struct_l_p_c___c_g_u___t.html#ae4d55c3e2485ac46e29efffa250d1fb1',1,'LPC_CGU_T']]],
  ['pll1_5fctrl_2647',['PLL1_CTRL',['../struct_l_p_c___c_g_u___t.html#a511841aab4fd9006722c9a4130315614',1,'LPC_CGU_T']]],
  ['pll1_5fstat_2648',['PLL1_STAT',['../struct_l_p_c___c_g_u___t.html#a2142c26f9eba82a8d115d2ab457f2287',1,'LPC_CGU_T']]],
  ['pll_5fctrl_2649',['PLL_CTRL',['../struct_c_g_u___p_l_l___r_e_g___t.html#ac44fd1b3cc9c8c43503c5f33bc249629',1,'CGU_PLL_REG_T']]],
  ['pll_5fmax_5fcco_5ffreq_2650',['PLL_MAX_CCO_FREQ',['../group___c_l_o_c_k__18_x_x__43_x_x.html#ga30bea6f91385c809e9b69f60bb76e463',1,'clock_18xx_43xx.h']]],
  ['pll_5fmdiv_2651',['PLL_MDIV',['../struct_c_g_u___p_l_l___r_e_g___t.html#a033ea2de72ec1feb7144e22c774d3a90',1,'CGU_PLL_REG_T']]],
  ['pll_5fmin_5fcco_5ffreq_2652',['PLL_MIN_CCO_FREQ',['../group___c_l_o_c_k__18_x_x__43_x_x.html#ga1b275f6e0df5d06ec980f9da9e3a1f02',1,'clock_18xx_43xx.h']]],
  ['pll_5fnp_5fdiv_2653',['PLL_NP_DIV',['../struct_c_g_u___p_l_l___r_e_g___t.html#ada13d18ce74e2c10d0e1e6ebb08091ed',1,'CGU_PLL_REG_T']]],
  ['pll_5fparam_5ft_2654',['PLL_PARAM_T',['../struct_p_l_l___p_a_r_a_m___t.html',1,'']]],
  ['pll_5fstat_2655',['PLL_STAT',['../struct_c_g_u___p_l_l___r_e_g___t.html#a8bc6e383ac2cac7fe5c0e4f9ac0a4fed',1,'CGU_PLL_REG_T']]],
  ['pm_2656',['PM',['../struct_l_p_c___c_c_u1___t.html#a3d44c3611cd3eaefe7d7214409ef00b8',1,'LPC_CCU1_T::PM()'],['../struct_l_p_c___c_c_u2___t.html#a3d44c3611cd3eaefe7d7214409ef00b8',1,'LPC_CCU2_T::PM()']]],
  ['pmc_5fdeeppowerdown_2657',['PMC_DeepPowerDown',['../group___p_m_c__18_x_x__43_x_x.html#gga69f642b5034785c56bb9c45e4dd780dea9076408a570dc195ff5615835a93d678',1,'pmc_18xx_43xx.h']]],
  ['pmc_5fdeepsleep_2658',['PMC_DeepSleep',['../group___p_m_c__18_x_x__43_x_x.html#gga69f642b5034785c56bb9c45e4dd780dea31d40a23015643fcc99a943b65841164',1,'pmc_18xx_43xx.h']]],
  ['pmc_5fpowerdown_2659',['PMC_PowerDown',['../group___p_m_c__18_x_x__43_x_x.html#gga69f642b5034785c56bb9c45e4dd780dea9ab5cbaebd32ff7ae5414220e7281b04',1,'pmc_18xx_43xx.h']]],
  ['pmc_5fpwr_5fdeep_5fsleep_5fmode_2660',['PMC_PWR_DEEP_SLEEP_MODE',['../group___p_m_c__18_x_x__43_x_x.html#gaca8a69edb54ddb4cb539fda3f7c123a9',1,'pmc_18xx_43xx.h']]],
  ['pmc_5fpwr_5fdeep_5fsleep_5fmode_5fno_5fio_2661',['PMC_PWR_DEEP_SLEEP_MODE_NO_IO',['../group___p_m_c__18_x_x__43_x_x.html#ga3696589b9d3f6ab6bb5bee7e44fcde76',1,'pmc_18xx_43xx.h']]],
  ['pol_2662',['POL',['../struct_l_p_c___l_c_d___t.html#a9037a11797290aef4ac48048c07e2e89',1,'LPC_LCD_T']]],
  ['port_2663',['port',['../structserial__config.html#a2fa54f9024782843172506fadbee2ac8',1,'serial_config::port()'],['../struct_s_p_i___address__t.html#a2fa54f9024782843172506fadbee2ac8',1,'SPI_Address_t::port()'],['../structspi_config__t.html#a0225abd4a82db41436db7417aac6873a',1,'spiConfig_t::port()']]],
  ['port_2664',['PORT',['../group___c_m_s_i_s__core___debug_functions.html#ga95d6dd68e2d2d252ef38c97738b31bd8',1,'ITM_Type']]],
  ['port_5fena_2665',['PORT_ENA',['../struct_l_p_c___g_p_i_o_g_r_o_u_p_i_n_t___t.html#a1adf1285b0f28fae9537b4cd71646145',1,'LPC_GPIOGROUPINT_T']]],
  ['port_5fmiso1_2666',['PORT_MISO1',['../spi_8c.html#a4e01e480acbe18712f1f04872274f1d0',1,'spi.c']]],
  ['port_5fmosi1_2667',['PORT_MOSI1',['../spi_8c.html#a33c77f2ef81471566b5182ea5ae1af08',1,'spi.c']]],
  ['port_5fpol_2668',['PORT_POL',['../struct_l_p_c___g_p_i_o_g_r_o_u_p_i_n_t___t.html#a31a0cb631ddc723b8277b38f7c169cc9',1,'LPC_GPIOGROUPINT_T']]],
  ['port_5fsck1_2669',['PORT_SCK1',['../spi_8c.html#acf40fb602fef2a392a65dcc6e01b8937',1,'spi.c']]],
  ['portsc1_5fd_2670',['PORTSC1_D',['../struct_l_p_c___u_s_b_h_s___t.html#a3f95ae68d823fd2a4f4fc31381c092c5',1,'LPC_USBHS_T']]],
  ['portsc1_5fh_2671',['PORTSC1_H',['../struct_l_p_c___u_s_b_h_s___t.html#a793643c9430524a34502b27c87132339',1,'LPC_USBHS_T']]],
  ['pos_2672',['POS',['../struct_l_p_c___q_e_i___t.html#ad349c0799c073bca3bdc59ca2f1f2ffd',1,'LPC_QEI_T']]],
  ['power_5fcontrol_2673',['POWER_CONTROL',['../struct_l_p_c___h_s_a_d_c___t.html#a8f1df094209082e7487a99d38134617a',1,'LPC_HSADC_T']]],
  ['power_5fdown_2674',['POWER_DOWN',['../struct_l_p_c___h_s_a_d_c___t.html#a66a06f743e4b9dd8a59503fd111e42e5',1,'LPC_HSADC_T']]],
  ['ppl_2675',['PPL',['../struct_l_c_d___c_o_n_f_i_g___t.html#a34a9e7f684990f3d8c924b0b7ad060b1',1,'LCD_CONFIG_T']]],
  ['ppsctrl_2676',['PPSCTRL',['../struct_l_p_c___e_n_e_t___t.html#ad8f64bc7ac1581c660e982efe0763c3c',1,'LPC_ENET_T']]],
  ['pr_2677',['PR',['../struct_l_p_c___t_i_m_e_r___t.html#af8d25514079514d38c104402f46470af',1,'LPC_TIMER_T']]],
  ['preset_2678',['PRESET',['../struct_l_p_c___a_t_i_m_e_r___t.html#a0d1aa425e85140fab9caba063ae0b5fa',1,'LPC_ATIMER_T']]],
  ['ps_5fpower_5ffunc_5ft_2679',['PS_POWER_FUNC_T',['../group___s_d_i_f__18_x_x__43_x_x.html#ga9f9a94c5b4812b8fdc5aa4a808e9ceaf',1,'sdif_18xx_43xx.h']]],
  ['pscheck_5ffunc_5ft_2680',['PSCHECK_FUNC_T',['../group___s_d_i_f__18_x_x__43_x_x.html#ga3a32ffbe111e1b16b9efd08aee03727e',1,'sdif_18xx_43xx.h']]],
  ['psdmmc_5fdma_5ft_2681',['pSDMMC_DMA_T',['../structp_s_d_m_m_c___d_m_a___t.html',1,'']]],
  ['psel_2682',['psel',['../struct_p_l_l___p_a_r_a_m___t.html#a372f2e371b89055c7bd0ce0b1cbb7999',1,'PLL_PARAM_T']]],
  ['pserial_2683',['pSerial',['../structserial__config.html#a1944cd6d24e8b238d8e728d0cf201541',1,'serial_config']]],
  ['ptr_5fgpio_5fgroup_5fint_5ffunc_2684',['ptr_GPIO_group_int_func',['../group___g_p_i_o.html#gadb1b43449a7ec81462b1e8ae68041b50',1,'gpio.c']]],
  ['ptr_5fgpio_5fint_5ffunc_2685',['ptr_GPIO_int_func',['../group___g_p_i_o.html#gac7d9672849de0a3c41c38280af236661',1,'gpio.c']]],
  ['pwrdwn_2686',['PWRDWN',['../struct_l_p_c___e_e_p_r_o_m___t.html#a8da250a9c7b3373527699c56b6f9671c',1,'LPC_EEPROM_T']]],
  ['pwren_2687',['PWREN',['../struct_l_p_c___s_d_m_m_c___t.html#a4013fc9eef95c3aaafd95aee438455c6',1,'LPC_SDMMC_T']]]
];
