var group___i_n_a219 =
[
    [ "INA219_Calibrate", "group___i_n_a219.html#gad5557cca992c052ea7d21606e890df1e", null ],
    [ "INA219_Configurate", "group___i_n_a219.html#gacf2ae10494a43c440e09b5bb316c041e", null ],
    [ "INA219_GetCurrent", "group___i_n_a219.html#gab8abb68998451605d7ffe6ff63c1c13d", null ],
    [ "INA219_GetCurrentLSB", "group___i_n_a219.html#ga6e0e1babe552be5c4949b8ced1f8fc65", null ],
    [ "INA219_GetPower", "group___i_n_a219.html#ga44a5a3c2d6194a659da522e73a4eb83d", null ],
    [ "INA219_GetVbus", "group___i_n_a219.html#ga3ea0114ca9d8591e07bfc2e066f69f12", null ],
    [ "INA219_GetVshunt", "group___i_n_a219.html#gab20ec8cfd18093d45c3c4bdc3c04e403", null ],
    [ "INA219_InitI2CBus", "group___i_n_a219.html#gacf842c6d67245b235a5df10df10463e7", null ],
    [ "INA219_ReadCalibrationRegister", "group___i_n_a219.html#gad61cfcbb9829efb6f5a5910c9842e5a4", null ],
    [ "INA219_ReadConfigRegister", "group___i_n_a219.html#gac05e3562a36256f0af7664d3bad186da", null ],
    [ "INA219_ReadRegister", "group___i_n_a219.html#gabd75aa85db841e20cf09601ac6940481", null ],
    [ "INA219_SetRegister", "group___i_n_a219.html#ga5b81fffe486d121e5f1672a06613d558", null ]
];