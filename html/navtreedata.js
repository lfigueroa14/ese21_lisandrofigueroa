/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Carga Electrónica", "index.html", [
    [ "Inicio", "index.html", [
      [ "Driver INA219 para LPC4337", "index.html#Documentación", [
        [ "Control de Cambios", "index.html#subSec01", null ]
      ] ]
    ] ],
    [ "MISRA-C:2004 Compliance Exceptions", "_c_m_s_i_s__m_i_s_r_a__exceptions.html", null ],
    [ "README", "md__r_e_a_d_m_e.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_m_s_i_s__m_i_s_r_a__exceptions.html",
"group___a_e_s__18_x_x__43_x_x.html#ggae6ec18c3dfb817d9b4440eaaf6edd3e5a81bf8f9bd1f4847c05c8ce5c93f1dc1b",
"group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a7601b42a319f3767dde4e08ff6e613c0",
"group___c_m_s_i_s___d_w_t.html#ga6c12e2868b8989a69445646698b8c331",
"group___c_m_s_i_s___s_c_b.html#ga89a28cc31cfc7d52d9d7a8fcc69c7eac",
"group___c_m_s_i_s___t_p_i.html#ga80ecae7fec479e80e583f545996868ed",
"group___c_m_s_i_s__core___debug_functions.html#ga711f336367372393a5f874e5c46e2b95",
"group___c_m_s_i_s__core___debug_functions.html#gad6abd8c7878d64e5e8e442de842f9de8",
"group___e_m_c__18_x_x__43_x_x.html#ga75312a857f7b6623919b516996e2a977",
"group___e_n_e_t__18_x_x__43_x_x.html#gaf7079eadf07a217977bc5d90eca5ee21",
"group___g_p_d_m_a__18_x_x__43_x_x.html#gga2f4aa97bd0ffa5046c8e2b17028d99ccaee7927c433e007f270c365bcca865706",
"group___i2_c__18_x_x__43_x_x.html#gae90c077796281121b65b80b661a23062",
"group___l_p_c___types___public___types.html#ga5f7b826e88ffb1fae2260abb3a75b19a",
"group___s_c_t__18_x_x__43_x_x.html#gga642884ed46b523a1106a6505950e0259a37d0cb43a4c9dc34fd618481c82dfe87",
"group___s_p_i.html#gga8eca2297218426636952c631c9a8c881a0cb4f6c37501162dcdcd5686e2efea9a",
"group___u_a_r_t__18_x_x__43_x_x.html#ga31933a99dfe9a8afac45c1f26b0cf021",
"group___u_s_b_d___core.html#gaa7808914116fe45223462e68d6f8aa5d",
"group___u_s_b_d___h_i_d.html#ga8ed6d2977991debbd329f8b0f62700f7",
"struct___c_d_c___h_e_a_d_e_r___d_e_s_c_r_i_p_t_o_r.html#a65d7922f01732e3c44ebb46a73f99393",
"struct_e_n_e_t___t_x_d_e_s_c___t.html",
"struct_l_p_c___e_m_c___t.html#a766ff0c9eff7cbd7d9d00b7c8f055397",
"struct_l_p_c___o_t_p___t.html#aabc2d34f70cbb023181caa7382a0531a",
"struct_l_p_c___u_s_a_r_t___t.html#a982bb8011be2ffc1dd0d4379012b816e",
"struct_u_s_b_d___m_s_c___i_n_i_t___p_a_r_a_m.html#a1ef0815a72f7fb693a902e4a737a13c9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';