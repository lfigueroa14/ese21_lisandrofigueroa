var group___analog___i_o =
[
    [ "analog_input_config", "structanalog__input__config.html", [
      [ "input", "structanalog__input__config.html#a2475c78015c7393c1c9939ab5a57ad9f", null ],
      [ "mode", "structanalog__input__config.html#a37e90f5e3bd99fac2021fb3a326607d4", null ],
      [ "pAnalogInput", "structanalog__input__config.html#a2298b64ab1835d56f927cb94137f36ad", null ]
    ] ],
    [ "CH0", "group___analog___i_o.html#gaf6f592bd18a57b35061f111d32a7f637", null ],
    [ "AnalogInputInit", "group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de", null ],
    [ "AnalogInputReadPolling", "group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9", null ],
    [ "AnalogOutputInit", "group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a", null ],
    [ "AnalogOutputWrite", "group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c", null ],
    [ "AnalogStartConvertion", "group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368", null ]
];