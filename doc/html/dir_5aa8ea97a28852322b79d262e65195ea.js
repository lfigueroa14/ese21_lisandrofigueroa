var dir_5aa8ea97a28852322b79d262e65195ea =
[
    [ "analog_io.h", "analog__io_8h.html", "analog__io_8h" ],
    [ "bool.h", "drivers__microcontroller_2inc_2bool_8h.html", "drivers__microcontroller_2inc_2bool_8h" ],
    [ "gpio.h", "gpio_8h.html", "gpio_8h" ],
    [ "i2c.h", "i2c_8h.html", "i2c_8h" ],
    [ "spi.h", "spi_8h.html", "spi_8h" ],
    [ "systemclock.h", "systemclock_8h.html", "systemclock_8h" ],
    [ "timer.h", "timer_8h.html", "timer_8h" ],
    [ "uart.h", "uart_8h.html", "uart_8h" ]
];