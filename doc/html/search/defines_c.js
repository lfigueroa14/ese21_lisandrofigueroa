var searchData=
[
  ['periodo_9670',['PERIODO',['../ecg_8c.html#a3085f4877388c18954b353865f58cd56',1,'ecg.c']]],
  ['pin_5fmiso1_9671',['PIN_MISO1',['../spi_8c.html#abdca583a8d933ff14102c86c9b4ade9e',1,'spi.c']]],
  ['pin_5fmosi1_9672',['PIN_MOSI1',['../spi_8c.html#ac18998ada9e5b2df56ff411c5891f3dc',1,'spi.c']]],
  ['pin_5fsck1_9673',['PIN_SCK1',['../spi_8c.html#afd05c5f0e0b7221c6a51b296a657e3f7',1,'spi.c']]],
  ['port_5fmiso1_9674',['PORT_MISO1',['../spi_8c.html#a4e01e480acbe18712f1f04872274f1d0',1,'spi.c']]],
  ['port_5fmosi1_9675',['PORT_MOSI1',['../spi_8c.html#a33c77f2ef81471566b5182ea5ae1af08',1,'spi.c']]],
  ['port_5fsck1_9676',['PORT_SCK1',['../spi_8c.html#acf40fb602fef2a392a65dcc6e01b8937',1,'spi.c']]],
  ['pr_5fzero_9677',['PR_ZERO',['../delay_8c.html#a3b543f16b4a382f92c47a7bf9865dd41',1,'delay.c']]]
];
