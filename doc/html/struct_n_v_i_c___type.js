var struct_n_v_i_c___type =
[
    [ "IABR", "group___c_m_s_i_s__core___debug_functions.html#gac8694e172f431db09b5d570993da3917", null ],
    [ "ICER", "group___c_m_s_i_s__core___debug_functions.html#gaf458bc93cfb899fc1c77c5d1f39dde88", null ],
    [ "ICPR", "group___c_m_s_i_s__core___debug_functions.html#ga8165d9a8c0090021e56bbe91c2c44667", null ],
    [ "IP", "group___c_m_s_i_s__core___debug_functions.html#ga38c377984f751265667317981f101bb4", null ],
    [ "ISER", "group___c_m_s_i_s__core___debug_functions.html#ga0bf79013b539f9f929c75bd50f8ec67d", null ],
    [ "ISPR", "group___c_m_s_i_s__core___debug_functions.html#gab39acf254b485e3ad71b18aa9f1ca594", null ],
    [ "RESERVED0", "group___c_m_s_i_s__core___debug_functions.html#gac881b676be4d9659951f43c2fccb34b4", null ],
    [ "RESERVED2", "group___c_m_s_i_s__core___debug_functions.html#ga86dfd6bf6c297be163d078945f67e8b6", null ],
    [ "RESERVED3", "group___c_m_s_i_s__core___debug_functions.html#ga3371761ff5e69fb1b6b3c2c3b4d69b18", null ],
    [ "RESERVED4", "group___c_m_s_i_s__core___debug_functions.html#ga0c75e6c517dc8e5596ffa3ef6285c232", null ],
    [ "RESERVED5", "group___c_m_s_i_s__core___debug_functions.html#ga77017390737a14d5eb4cdb41f0aa3dce", null ],
    [ "RSERVED1", "group___c_m_s_i_s__core___debug_functions.html#gab993fe7f0b489b30bc677ccf53426a92", null ],
    [ "STIR", "group___c_m_s_i_s__core___debug_functions.html#ga471c399bb79454dcdfb342a31a5684ae", null ]
];