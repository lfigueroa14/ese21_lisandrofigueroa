/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Carga Electrónica", "index.html", [
    [ "Inicio", "index.html", [
      [ "Driver INA219 para LPC4337", "index.html#Documentación", [
        [ "Control de Cambios", "index.html#subSec01", null ]
      ] ]
    ] ],
    [ "MISRA-C:2004 Compliance Exceptions", "_c_m_s_i_s__m_i_s_r_a__exceptions.html", null ],
    [ "README", "md__home_lisandro__documentos__e_s_e__arquitectura_y__programacion_de__sistemas__embebidos_tp_final_firmware__r_e_a_d_m_e.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_m_s_i_s__m_i_s_r_a__exceptions.html",
"cr__start__m0_8h.html#af55b0ec4b835931b70d29a0c70a26160",
"error_8h.html#a905255056c349318139d94aa4523d516a93e8e371d0add67127ed6276e7a53673",
"group___a_d_c__18_x_x__43_x_x.html#ga182ae98a23007564b3eaeb61a31ac553",
"group___analog___i_o.html#gad13e6436e0177f0e17dc1a01fc4d47af",
"group___c_h_i_p___s_d_m_m_c___definitions.html#ga16f975fa60cdd20c9ec28984b7963213",
"group___c_h_i_p___s_d_m_m_c___definitions.html#gga1bf27ef97c5e846aa2b0f8372583502aaa59870a4a4a750b54215771d5407e6a0",
"group___c_m_s_i_s__43_x_x___i_r_q.html#gga2018a6433701e9ee9b34797425127919a5c766577f50ed37f5833a60ca2ee6685",
"group___c_m_s_i_s___d_w_t.html#ga031c693654030d4cba398b45d2925b1d",
"group___c_m_s_i_s___s_c_b.html#ga3cba2ec1f588ce0b10b191d6b0d23399",
"group___c_m_s_i_s___t_p_i.html#ga0c799ff892af5eb3162d152abc00af7a",
"group___c_m_s_i_s__core___debug_functions.html#ga4bdbe4ff58983d940ca72d8733feaedd",
"group___c_m_s_i_s__core___debug_functions.html#gac3ee2c30a1ac4ed34c8a866a17decd53",
"group___c_o_m_m_o_n___i_a_p.html#ga8d15cbc501933748afbe786c587e74f9",
"group___e_m_c__18_x_x__43_x_x.html#ga4d9286ae1156d93abdabdb3a0e22ba01",
"group___e_n_e_t__18_x_x__43_x_x.html#ga3faccf9a5f5bc2dcf47a52fcac3ab7ab",
"group___e_n_e_t__18_x_x__43_x_x.html#gadf77e5987d77a71ad4ce3b332855ff54",
"group___g_i_o_p.html#gga6baa390d39e68032f8ebed93a7039be1a792d19419bb5688050556ed9a76b6697",
"group___g_p_i_o.html#ga249d7a560045daf5efd327072eec8669",
"group___h_s_a_d_c__18_x_x__43_x_x.html#gab8577fad41f87c7822073b4b0e1b40c3",
"group___i2_c__18_x_x__43_x_x.html#gad8c325e3c58aa8e7810797b9a6761596",
"group___i2_s__18_x_x__43_x_x.html#gac08890ba38fd8e5df3a3a603e7a4fa42",
"group___i_n_a219.html#gacf842c6d67245b235a5df10df10463e7",
"group___o_t_p__18_x_x__43_x_x.html#gab94e93611ec4efa732ad045c471d646c",
"group___p_e_r_i_p_h__43_x_x___b_a_s_e.html#ga09c4610ada1d9aa18913963cbd1a6e52",
"group___p_i_n_i_n_t__18_x_x__43_x_x.html#gab1378b396f6d1ba5a192153b40504353",
"group___r_t_c__18_x_x__43_x_x.html#ga5002ab062a5871b7ed73c7ff0cb00e6b",
"group___s_c_t__18_x_x__43_x_x.html#ga19527730d0cdbf041987ab3510a0ee16",
"group___s_d_i_f__18_x_x__43_x_x.html#ga072fae306e8eefc095a7c3d68d614892",
"group___s_d_i_f__18_x_x__43_x_x.html#gaf31ae80f99b27ee56f4ae341670058ca",
"group___s_p_i.html#gab8c13a643d448d494cec07376efe1757",
"group___s_s_p__18_x_x__43_x_x.html#gga59cc14c5381f32c16286c45bd6a3ece0acf79b0a911e1c9d4cee5387be0dc45ba",
"group___timer.html#ga31487bffd934ce838a72f095f9231b24",
"group___u_a_r_t__18_x_x__43_x_x.html#ga85050a24048ffc2de997cd60ea67f9df",
"group___u_s_b_d___core.html#ga7c2da119cc5c129d253d5fa766c70893",
"group___u_s_b_d___h_i_d.html#ga3a9ec4aee050a3345dab774f559f06b6",
"group___u_s_b_d___h_i_d.html#gac8a9a7f08802f62c76663f2e4d42ce20",
"i2c__18xx__43xx_8h.html#ga21aa839302786105dcf6a96be0e6e8bca674732e92811621ed2ca34b9e40ca724",
"struct___m_s_c___c_s_w.html#a89831a21c46d192c94b3599a1c476af2",
"struct_i_p___e_m_c___d_y_n___c_o_n_f_i_g___t.html",
"struct_l_p_c___e_n_e_t___t.html#a509bec0d8aa03269d5e7f53eadad7fa0",
"struct_l_p_c___q_e_i___t.html#ad2d983a1db30704acc69ddc99cedbe86",
"struct_l_p_c___u_s_b_h_s___t.html#ae39ae814acf9cd8199acd30e6603e6ff",
"structspi_config__t.html#a32678c62007e0748dae550bd1007a07d",
"usbd__adc_8h.html#ab34a2bc913fe89452fae9464dd1c9eda",
"usbd__hiduser_8h_source.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';