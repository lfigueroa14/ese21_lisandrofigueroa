/* Copyrigth 2021,
 * Lisandro Figueroa
 * lisandrofigueroa@gmail.com
 * Argentina
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * LF		Lisandro Figueroa
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210921 v0.0.1 first commit
 */



/*==================[inclusions]=============================================*/
#include "ecg.h"
#include "systemclock.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"



/*==================[macros and definitions]=================================*/

#define PERIODO 10		// mientras más chico más rápido se ve en serialplot

/*==================[internal data definition]===============================*/

const uint8_t ECG1[]={
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17
};


uint16_t i=0; //indice para el vector
uint16_t Pote=0; //valor lectura pote

/*==================[internal functions declaration]=========================*/
void enviarSenal(void);
void procesarSenal(void);
void ftdi(void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,PERIODO,&enviarSenal};
analog_input_config adc = {CH1, AINPUTS_SINGLE_READ,&procesarSenal};
serial_config usart = {SERIAL_PORT_PC, 115200, &ftdi};


/*==================[external functions definition]==========================*/

void enviarSenal(void)
{
	uint32_t ecg;
	float aux;
	AnalogStartConvertion();
	if (i <= sizeof(ECG1))
			ecg=ECG1[i++];
	else {
		i=0;
		ecg=ECG1[i++];
	}
	//Pote = Pote / 4; // divido por 4 para limitar a 8 bits el valor del ADC
	aux = ecg * (Pote/255);
	ecg = (int)aux;
	//UartSendString(SERIAL_PORT_PC, UartItoa(Pote, 10));
	UartSendString(SERIAL_PORT_PC, UartItoa(ecg, 10));
	UartSendString(SERIAL_PORT_PC, "\n");

}

void procesarSenal(void){
//leer el valor del POTE y cargar en valPot
	AnalogInputRead(CH1,&Pote);

}

void ftdi(void){
	//
};


void SisInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);
    TimerStart(TIMER_A);
	LedsInit(); //inicializar led en OFF
	UartInit(&usart); //inicializar USART2
	AnalogInputInit(&adc); //inicializar ADC

}

int main(void)
{
	SisInit();

	while(1){}


}

/*==================[end of file]============================================*/

