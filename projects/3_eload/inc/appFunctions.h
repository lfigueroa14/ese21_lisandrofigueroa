/*
 * userFunciones.h
 *
 *  Created on: 8 nov. 2021
 *      Author: Lisandro
 */

#ifndef PROJECTS_3_ELOAD_INC_APPFUNCTIONS_H_
#define PROJECTS_3_ELOAD_INC_APPFUNCTIONS_H_

void delayTMRms(uint16_t d);
float readTemp(uint16_t *atemp);
void printFloat(float data);
bool readCmd(char *buff, uint16_t *iset, uint8_t *steps, uint16_t *tsweep);
void setCurrent(uint16_t iset, float Imax, float Rsh);
void sweepCurrent(float Imax, float Rsh, uint16_t iset, uint8_t steps, uint16_t tsweep, uint16_t *Atemp);

#endif /* PROJECTS_3_ELOAD_INC_APPFUNCTIONS_H_ */
