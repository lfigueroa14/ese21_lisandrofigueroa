/*
 * eload.h
 *
 *  Created on: 28 oct. 2021
 *      Author: Lisandro
 */

#ifndef PROJECTS_3_ELOAD_INC_ELOAD_H_
#define PROJECTS_3_ELOAD_INC_ELOAD_H_

const uint32_t CLOCK_I2C = 50000;		// 50KHz
const uint8_t PERIODO = 1;				// 1ms TMR

// Registro de configuración chip INA219 //
#define CONFIG_VAL_1   (CONTINUOUS_VSHUNT_VBUS|VSH_AVG_32|VBUS_AVG_32|PGA_4|VBUS_16V)

// Longitud comando a recibir por UART //
#define cmdSize	8

// Valores según el diseño del hardware //
const float I_MAX = 2;
const float R_SHUNT = 0.1;

#endif /* PROJECTS_3_ELOAD_INC_ELOAD_H_ */
