/*
 * eload.c
 *
 *  Created on: 28 oct. 2021
 *      Author: Lisandro
 */

#include "INA219.h"
#include "systemclock.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"
#include "eload.h"
//#include "../inc/appFunctions.h"
#include "appFunctions.h"

uint8_t cont_serie = 0;
uint8_t cmd[cmdSize];
uint8_t sizeCMD;
uint8_t estado = 0;
uint16_t iSet = 0;
uint8_t Steps;
uint16_t timeSweep;
uint16_t aTemp; //valor analógico MCP9700

uint16_t base_delay = 0;
uint16_t base_timeOut = 0;
uint8_t flag_cmd = 0;
uint8_t flag_timer = 0;

void isr_timer(void);
void ftdi(void);
void temp(void);
void task1(void);
void task2(void);

timer_config my_timer = {TIMER_A,PERIODO,&isr_timer};
serial_config usart = {SERIAL_PORT_PC, 115200, &ftdi};
analog_input_config adc = {CH1, AINPUTS_SINGLE_READ, &temp};

void isr_timer(void)
{
	base_delay++;
	base_timeOut++;
}

void ftdi(void)
{
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	cmd[cont_serie] = dato;
	cont_serie++;
	if(cont_serie == cmdSize)
        {
          cont_serie = 0;
          flag_cmd = 1;
        }
}

void temp(void)
{
	AnalogInputRead(CH1,&aTemp);
}
void SisInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);
    TimerStart(TIMER_A);
	LedsInit(); //inicializar led en OFF
	UartInit(&usart); //inicializar USART2
	AnalogInputInit(&adc); //inicializar ADC
	AnalogOutputInit();
	AnalogOutputWrite(0);
	fpuInit();
	INA219_InitI2CBus(I2C0, CLOCK_I2C);
	INA219_Calibrate(I2C0, DEF_ADDRESS, I_MAX, R_SHUNT);
	INA219_Configurate(I2C0, DEF_ADDRESS, CONFIG_VAL_1);
	INA219_GetCurrent(I2C0, DEF_ADDRESS, I_MAX);
}

int main(void)
{
	SisInit();

	sizeCMD = cmdSize;		// longitud del cmd disponible en módulo de funciones
	bool errorCmd;
	const uint16_t timeOut = 1000;

	while(1)
		{

		//****************** Time Out Comando ********************//

		if(base_timeOut > timeOut)
			{
			cont_serie = 0;
			base_timeOut = 0;
			}

		//****************** Lectura del comando ******************//

		if(flag_cmd == 1)
			{
			errorCmd = readCmd(&cmd, &iSet, &Steps, &timeSweep);
            if(errorCmd == FALSE)
            	{
            	UartSendString(SERIAL_PORT_PC, "\n\nComando incorrecto!\n\n");
            	}
			flag_cmd = 0;
			base_timeOut = 0;
			}

		//***** Tarea segun estado, determinado por el comando *****//

		if(estado == 1)           	// Set Corriente
		      {
			  task1();
			  estado = 0;
		      }else if(estado == 2) // Sweep Corriente
		        {
		    	task2();
		        estado = 0;
		        }
		}
}

void task1(void)
{
	float fAux;
	uint16_t aux;

	UartSendString(SERIAL_PORT_PC, "\n\n ***** MODO CORRIENTE CONSTANTE ***** \n");
	UartSendString(SERIAL_PORT_PC, "\nCorriente seteada = ");
	if(iSet == 0)UartSendString(SERIAL_PORT_PC, "0");
		else UartSendString(SERIAL_PORT_PC, UartItoa(iSet, 10));
	UartSendString(SERIAL_PORT_PC, " mA");

	setCurrent(iSet, I_MAX, R_SHUNT);
	fAux = INA219_GetCurrent(I2C0, DEF_ADDRESS, I_MAX);

	// Medición de corriente //
	UartSendString(SERIAL_PORT_PC, "\nCorriente medida = ");
	printFloat(fAux);
	UartSendString(SERIAL_PORT_PC, " mA");

	// Medidición de voltaje //
	fAux = INA219_GetVbus(I2C0, DEF_ADDRESS);
	fAux = fAux + INA219_GetVshunt(I2C0, DEF_ADDRESS);		//la fuente bajo prueba es Vtest = Vbus + Vshunt
	UartSendString(SERIAL_PORT_PC, "\nVoltaje medido = ");
	printFloat(fAux);
	UartSendString(SERIAL_PORT_PC, " mV\n");

	// Medición de temperatura //
	UartSendString(SERIAL_PORT_PC, "Temperatura = ");
	fAux = readTemp(&aTemp);
	printFloat(fAux);
	UartSendString(SERIAL_PORT_PC, " grad. C\n");
}

void task2(void)
{
	UartSendString(SERIAL_PORT_PC, "\n\n ***** MODO BARRIDO DE CORRIENTE ***** \n");
	sweepCurrent(I_MAX, R_SHUNT, iSet, Steps, timeSweep, &aTemp);
}
