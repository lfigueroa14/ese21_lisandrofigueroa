/*
 * userFunciones.c
 *
 *  Created on: 8 nov. 2021
 *      Author: Lisandro
 */

#include "uart.h"
#include "analog_io.h"
#include "INA219.h"
#include "stopwatch.h"

const float DIV = 24;			// relación del divisor resistivo a la salida del DAC

extern uint8_t sizeCMD;
extern uint8_t estado;
extern uint16_t base_delay;

void delayTMR(uint16_t d)
{
	base_delay = 0;
	while(base_delay < d);
}

float readTemp(uint16_t *atemp)
{
	float ftemp;

	AnalogStartConvertion();
	delayTMR(1);
	ftemp = (float)((3 * 100 * (*atemp) / 1023.0) - 50.0);		// Aref EDU CIAA aprox. 3V
	return ftemp;
}

void printFloat(float data)
{
	float dato;
	int aux;
	dato = data;

	aux = (int)dato;									// trunca y guarda en un int
	if(aux==0)UartSendString(SERIAL_PORT_PC, "0");		// UartItoa() no imprime el cero
	UartSendString(SERIAL_PORT_PC, UartItoa(aux, 10));	// imprime un int
	UartSendString(SERIAL_PORT_PC, ",");				// imprime el punto o coma
	dato = dato - aux;									// al dato original le resto la parte entera
	aux = (int)((float)(100*dato));						// parte decimal con dos dígitos a entero
	if(aux==0)UartSendString(SERIAL_PORT_PC, "0");		// UartItoa() no imprime el cero
	UartSendString(SERIAL_PORT_PC, UartItoa(aux, 10));	// imprime un int
}

bool readCmd(char *buff, uint16_t *iset, uint8_t *steps, uint16_t *tsweep)
{
	uint8_t crc;
	crc = (uint8_t)(buff[0] + buff[1] + buff[2] + buff[3] + buff[4] + buff[5]);	//calcula crc

	if(crc == buff[6])
	//if(buff[6] != 0)   // PARA PROBAR SIN CALCULAR EL CRC
    	{

		if(buff[0] == 'i' && buff[sizeCMD-1] == 'f')
        	{
			*iset = 0x0000 | (buff[1] << 8);	// Corriente H
			*iset = *iset + buff[2];			// Corriente L
            estado = 1;
            return TRUE;
            }else if(buff[0] == 's' && buff[sizeCMD-1] == 'f')
            	{
            	*iset = 0x0000 | (buff[1] << 8);	// Corriente H
            	*iset = *iset + buff[2];			// Corriente L
            	*steps = buff[3];
            	*tsweep = 0x0000 | (buff[4] << 8);	// time H
            	*tsweep = *tsweep + buff[5];		// time L
            	estado = 2;
                return TRUE;
            	}
    	}else
    		{
    		//comando no valido
    		estado = 0;
    		return FALSE;
    		}
}

void setCurrent(uint16_t iset, float Imax, float Rsh)
{
	uint16_t vDac;
	uint8_t cont;
	uint16_t iRead;
	uint16_t aux_t;
	const uint8_t error = 1;

	vDac = (uint16_t)((DIV * iset * Rsh * 1023) / 3300);   // 0 <= vDac <= 1023 // 1023 = 3300 mV

	cont = 0;
	while(cont < 50)
		{
		AnalogOutputWrite(vDac);
		delayTMR(30);				// espera que se establezca de la corriente
		iRead = (uint16_t)(INA219_GetCurrent(I2C0, DEF_ADDRESS, Imax));
		if( (iRead+error) < iset )
			{
			vDac++;
			}else if( (iRead-error) > iset )
				{
				vDac--;
				}else
					{
					break;	//es igual o dentro del margen de error
					}
		cont++;
		}
}

void sweepCurrent(float Imax, float Rsh, uint16_t iset, uint8_t steps, uint16_t tsweep, uint16_t *Atemp)
{
  //iset es la ifinal ingresada por el usuario
  int j;
  uint16_t istep = 0; 		// corriente del paso n
  uint16_t swStep;
  float fAux;
  swStep = iset / steps;

  UartSendString(SERIAL_PORT_PC, "\nI [mA] ; V[mV] ; T[grad. C]");

  for(j=0; j < steps; j++)
    {
	  istep = istep + swStep;
	  setCurrent(istep, Imax, Rsh);
	  delayTMR(tsweep);

	  // Mide corriente //
	  fAux = INA219_GetCurrent(I2C0, DEF_ADDRESS, Imax);
	  UartSendString(SERIAL_PORT_PC, "\n");
	  printFloat(fAux);

	  // Mide voltaje //
	  fAux = INA219_GetVbus(I2C0, DEF_ADDRESS);
	  fAux = fAux + INA219_GetVshunt(I2C0, DEF_ADDRESS);	//la fuente bajo prueba es Vtest = Vbus + Vshunt
	  UartSendString(SERIAL_PORT_PC, " ; ");
	  printFloat(fAux);

	  // Mide temperatura //
	  fAux = readTemp(Atemp);
	  UartSendString(SERIAL_PORT_PC, " ; ");
	  printFloat(fAux);
    }
  AnalogOutputWrite(0);		//setea en cero la corriente antes de retornar
}


