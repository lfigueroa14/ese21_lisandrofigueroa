/**
 * @mainpage Inicio
 *
 * @section Documentación Driver INA219 para LPC4337
 * 
 * @subsection subSec01 Control de Cambios
 *
 *  Fecha | User | Descripcion
 * : - - -- -:|: - - - -:|:- - - - - - - - - - - - :|
 * 28/10/2021 | Lisandro Figueroa | Creacion del driver
 * 05/10/2021 | Lisandro Figueroa | Driver testeado completamente
 * 15/07/2021 | Lisandro Figueroa | Documentacion con Doxygen
 *
 * @file INA219.c
 * @brief Cuerpo de Funciones
 * @version 1.0
 * @date 28/10/2021
 * @author Lisandro Figueroa
 *
 */

//#include "../inc/INA219.h"
#include "INA219.h"
#include "systemclock.h"
#include "chip.h"

void INA219_InitI2CBus(uint8_t I2C_N, uint32_t clock)
{
	/* Modo de trabajo del bus. El módulo I2C1 sólo soporta STANDARD FAST MODE,
	 * por lo que esta función es sólo para el puerto I2C0.
	 * Comentar el modo que no se usa.
	 */
	if(I2C_N == I2C0)
		{
			Chip_SCU_I2C0PinConfig(I2C0_STANDARD_FAST_MODE);
			//Chip_SCU_I2C0PinConfig(I2C0_FAST_MODE_PLUS);
		}
	/* Inicializacion del periferico */
    Chip_I2C_Init(I2C_N);

    /* Seleccion de velocidad del bus */
    Chip_I2C_SetClockRate(I2C_N, clock);

    /* EventHandler por polling */
    Chip_I2C_SetMasterEventHandler(I2C_N, Chip_I2C_EventHandlerPolling);
}

void INA219_Calibrate(uint8_t I2C_N, uint8_t DeviceAddress, float Imax, float Rshunt)
{
  uint16_t CalibrationValue;
  uint8_t dataBuffer[3];

  CalibrationValue = (int)((0.04096 * 32768)/(Imax * Rshunt));
  dataBuffer[0] = REG_CAL;
  dataBuffer[1] = (CalibrationValue & 0xFF00) >> 8;
  dataBuffer[2] = (CalibrationValue & 0xFF);

  Chip_I2C_MasterSend(I2C_N, DeviceAddress, dataBuffer, 3);
}

// CONFIGURAR //
// USAR MASCARAS CON OR PARA ARMAR ConfigValue
void INA219_Configurate(uint8_t I2C_N, uint8_t DeviceAddress, uint16_t ConfigValue)
{
  uint8_t dataBuffer[3];

  dataBuffer[0] = REG_CONFIG;
  dataBuffer[1] = (ConfigValue & 0xFF00) >> 8;
  dataBuffer[2] = (ConfigValue & 0xFF);

  Chip_I2C_MasterSend(I2C_N, DeviceAddress, dataBuffer, 3);
}

// SETEAR REGISTRO A LEER //
void INA219_SetRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t *RegAddress)
{
	Chip_I2C_MasterSend(I2C_N, DeviceAddress, RegAddress, 1);
}

// LEER REGISTRO //
void INA219_ReadRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t* buffer)
{
	Chip_I2C_MasterRead	(I2C_N, DeviceAddress, buffer, 2);
}

// Calcular current LSB //
float INA219_GetCurrentLSB(float Imax)
{
  float valor;
  valor = Imax / 32768;
  return valor;
}

float INA219_GetVshunt(uint8_t I2C_N, uint8_t DeviceAddress)
{
	// Vshunt_LSB = 10uV -> resultado en mV -> Reg_Vshunt * 0.01

	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_VSHUNT};
	int16_t valor;
	float fval;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];

	fval = valor * 0.01;

	return fval;
}

float INA219_GetVbus(uint8_t I2C_N, uint8_t DeviceAddress)
{
	// Vbus_LSB = 4mV -> resultado en mV ( * 0.004 para V)

	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_VBUS};
	uint16_t valor;
	float fval;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];
	valor = (valor >> 3) * 4;	// resultado en mV

	fval = valor * 1.0;

	return fval;
}

float INA219_GetPower(uint8_t I2C_N, uint8_t DeviceAddress, float Imax)
{
	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_POWER};
	uint16_t valor;
	float fval;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];

	fval = valor * 20 * INA219_GetCurrentLSB(Imax);		// resultado en W
														// podría almacenarse en una variable global cuando se llama
														// INA219_Calibrate()
	return fval;
}

float INA219_GetCurrent(uint8_t I2C_N, uint8_t DeviceAddress, float Imax)
{
	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_CURRENT};
	uint16_t valor;
	float fval;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];

	fval = (Imax * valor * 1000) / 32768.0;		// resultado en mA ( * 1000)

	return fval;
}

uint16_t INA219_ReadConfigRegister(uint8_t I2C_N, uint8_t DeviceAddress)
{
	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_CONFIG};
	uint16_t valor;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];

	return valor;
}

uint16_t INA219_ReadCalibrationRegister(uint8_t I2C_N, uint8_t DeviceAddress)
{
	uint8_t RegVal[2];
	uint8_t RegAddress[] = {REG_CAL};
	uint16_t valor;

	INA219_SetRegister(I2C_N, DeviceAddress, RegAddress);
	INA219_ReadRegister(I2C_N, DeviceAddress, RegVal);

	valor = RegVal[0] << 8;
	valor = valor + RegVal[1];

	return valor;
}

