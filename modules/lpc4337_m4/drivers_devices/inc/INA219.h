/**
 *
 * @file funciones.h
 * @brief Prototipos de funciones
 * @date 05/11/2021
 * @author Lisandro Figueroa
 *
 */

/**
 * @addtogroup INA219
* 	Modulo de Funciones
* @{
*/

#ifndef INA219_H
#define INA219_H

#include "stdint.h"
#include "stdio.h"

/* Ver opción de guardar I2C_N en variable global */

//********* Constantes simbólicas y máscaras **********//

// I2C Default Address //
#define DEF_ADDRESS   0x40   // 1000000 (A0=GND, A1=GND)

// Mapa de registros INA219 //
#define REG_CONFIG    0x00
#define REG_VSHUNT    0x01
#define REG_VBUS      0x02
#define REG_POWER     0x03
#define REG_CURRENT   0x04
#define REG_CAL       0x05

//*************************CONFIGURATION MASKS***************************//

// Configuration register is set 0x399F on power and RST

//*** BIT0 TO BIT2: MODE SETTINGS ***//

#define POWER_DOWN_INA			0	// 000
#define TRIGGER_VSHUNT          1	// 001
#define TRIGGER_VBUS            2	// 010
#define TRIGGER_VSHUNT_VBUS     3	// 011
#define ADC_OFF                 4	// 100
#define CONTINUOUS_VSHUNT       5	// 101
#define CONTINUOUS_VBUS         6	// 110
#define CONTINUOUS_VSHUNT_VBUS  7 	// 111 (DEFAULT)

//*** BIT3 TO BIT6: VSHUNT ADC SETTINGS ***//

#define VSH_9_BIT     32    	//  100|000
#define VSH_10_BIT    40    	//  101|000
#define VSH_11_BIT    48    	//  110|000
#define VSH_12_BIT    56    	//  111|000	(DEFAULT)
#define VSH_12_BIT_1  64    	//  111|000
#define VSH_AVG_2     72    	// 1001|000
#define VSH_AVG_4     80    	// 1010|000
#define VSH_AVG_8     88    	// 1011|000
#define VSH_AVG_16    96    	// 1100|000
#define VSH_AVG_32    104   	// 1101|000
#define VSH_AVG_64    112   	// 1110|000
#define VSH_AVG_128   120   	// 1111|000

//*** BIT7 TO BIT10: VBUS ADC SETTINGS ***//

#define VBUS_9_BIT     512 		//  100|0000|000
#define VBUS_10_BIT    640 		//  101|0000|000
#define VBUS_11_BIT    768 		//  110|0000|000
#define VBUS_12_BIT    896 		//  111|0000|000 (DEFAULT)
#define VBUS_12_BIT_1  1024 	// 1000|0000|000
#define VBUS_AVG_2     1152 	// 1001|0000|000
#define VBUS_AVG_4     1280		// 1010|0000|000
#define VBUS_AVG_8     1408		// 1011|0000|000
#define VBUS_AVG_16    1536		// 1100|0000|000
#define VBUS_AVG_32    1664 	// 1101|0000|000
#define VBUS_AVG_64    1792 	// 1110|0000|000
#define VBUS_AVG_128   1920 	// 1111|0000|000

//*** BIT11 TO BIT12: PGA SETTINGS ***//

#define PGA_1    	0			// 00|0000|0000|000
#define PGA_2     2048			// 01|0000|0000|000
#define PGA_4     4096			// 10|0000|0000|000
#define PGA_8     6144			// 11|0000|0000|000 (DEFAULT)

//*** BIT13: VBUS RANGE ***//

#define VBUS_16V    0	 		// 0|00|0000|0000|000
#define VBUS_32V	8192 		// 1|00|0000|0000|000 (DEFAULT)

//*** BIT 14: NOT USED ***//

//*** BIT15: RESET BIT ***//

#define RST_CONFIG	16384		// 1|0|00|0000|0000|000

//***********************************************************************//


//********************* Prototipos de funciones *************************//

/**
* @fn void INA219_InitI2CBus(uint8_t I2C_N, uint32_t clock)
* @brief Inicializa el bus I2C a utilizar
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] clock entero sin signo. Frecuencia del clcok del bus I2C.
* @return Nada
*/
void INA219_InitI2CBus(uint8_t I2C_N, uint32_t clock);

/**
* @fn void INA219_Calibrate(uint8_t I2C_N, uint8_t DeviceAddress, float Imax, float Rshunt)
* @brief Calibra el INA219.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [float] Imax. Corriente máxima que circulará por el sistema.
* @param [float] Rshunt. Valor del shunt usado en el sistema.
* @return Nada
*/
void INA219_Calibrate(uint8_t I2C_N, uint8_t DeviceAddress, float Imax, float Rshunt);

/**
* @fn void INA219_Configurate(uint8_t I2C_N, uint8_t DeviceAddress, uint16_t ConfigValue)
* @brief Configura el INA219.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [in] ConfigValue entero sin signo, parámetro de configuración. Se lo puede construir utilizando las máscaras incluidas y operadores OR.
* @return Nada
*/
void INA219_Configurate(uint8_t I2C_N, uint8_t DeviceAddress, uint16_t ConfigValue);

/**
* @fn void INA219_SetRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t *RegAddress)
* @brief Setea el registro interno del INA219 que se va a leer.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [in] RegAddress entero sin signo. Dirección del registro interno del INA219 que se desea setear para luego leerlo.
* @return Nada
*/
void INA219_SetRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t *RegAddress);

/**
* @fn void INA219_ReadRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t* buffer)
* @brief Lee el registro interno del INA219 que fue seleccionado previamente mediante INA219_SetRegister.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [in] *buffer puntero a array que contendrá los datos leídos.
* @return Nada
*/
void INA219_ReadRegister(uint8_t I2C_N, uint8_t DeviceAddress, uint8_t* buffer);

/**
* @fn float INA219_GetCurrentLSB(float Imax)
* @brief Calcula CurrentLSB. Valor requerido por GetPower.
* @param [float] Imax. Corriente máxima que circulará por el sistema.
* @return [float] resultado con el valor CurrentLSB.
*/
float INA219_GetCurrentLSB(float Imax);

/**
* @fn float INA219_GetVshunt(uint8_t I2C_N, uint8_t DeviceAddress)
* @brief Lee el registro del INA219 de Voltaje del shunt del INA219. Debe llamarse previamente a INA219_SetRegister con el RegAddress correspondiente.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @return [float] Voltaje del shunt en mV.
*/
float INA219_GetVshunt(uint8_t I2C_N, uint8_t DeviceAddress);

/**
* @fn float INA219_GetVbus(uint8_t I2C_N, uint8_t DeviceAddress)
* @brief Lee el registro del INA219 de Voltaje del bus. Debe llamarse previamente a INA219_SetRegister con el RegAddress correspondiente.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @return [float] Voltaje del bus en mV.
*/
float INA219_GetVbus(uint8_t I2C_N, uint8_t DeviceAddress);

/**
* @fn float INA219_GetPower(uint8_t I2C_N, uint8_t DeviceAddress, float Imax)
* @brief Lee el registro del INA219 de Potencia. Debe llamarse previamente a INA219_SetRegister con el RegAddress correspondiente.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [float] Imax. Corriente máxima que circulará por el sistema.
* @return [float] Potencia en Watts.
*/
float INA219_GetPower(uint8_t I2C_N, uint8_t DeviceAddress, float Imax);

/**
* @fn float INA219_GetCurrent(uint8_t I2C_N, uint8_t DeviceAddress, float Imax)
* @brief Lee el registro del INA219 de Corriente. Debe llamarse previamente a INA219_SetRegister con el RegAddress correspondiente.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @param [float] Imax. Corriente máxima que circulará por el sistema.
* @return [float] Corriente en mA.
*/
float INA219_GetCurrent(uint8_t I2C_N, uint8_t DeviceAddress, float Imax);

/**
* @fn uint16_t INA219_ReadConfigRegister(uint8_t I2C_N, uint8_t DeviceAddress)
* @brief Lee el registro interno del INA219 de Configuración.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @return [in] entero sin signo con el valor contenido en el registro
*/
uint16_t INA219_ReadConfigRegister(uint8_t I2C_N, uint8_t DeviceAddress);

/**
* @fn uint16_t INA219_ReadCalibrationRegister(uint8_t I2C_N, uint8_t DeviceAddress)
* @brief Lee el registro interno del INA219 de Calibración.
* @param [in] I2C_N entero sin signo. Módulo I2C a utilizar.
* @param [in] DeviceAddress entero sin signo. Dirección del INA219 en el bus I2C.
* @return [in] entero sin signo con el valor contenido en el registro
*/
uint16_t INA219_ReadCalibrationRegister(uint8_t I2C_N, uint8_t DeviceAddress);

//***********************************************************************//

/*! @} cierre de grupo INA219 */

#endif /* #ifndef INA219_H */
